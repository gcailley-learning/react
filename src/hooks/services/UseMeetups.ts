import { useCallback, useState } from "react";
import { MeetupItemModel } from "../../models/MeetupItemModel";
import useHttp from "./UseHttp";

export const useMeetups = () => {
  const transformMeetups = useCallback((data: any) => {
    const meetups = [];
    for (const key in data) {
      const meetup = { id: key, ...data[key] };
      meetups.push(meetup);
    }
    setMeetups(meetups);
  }, []);

  const {
    load: fetchMeetups,
    post: postMeetups,
    isLoading,
    error: error,
  } = useHttp("meetups", transformMeetups);
  const [meetups, setMeetups] = useState<MeetupItemModel[]>([]);

  return { fetchMeetups, postMeetups, meetups, isLoading, error };
};

export default useMeetups;
