import { useCallback, useState } from "react";
import env from "../../configuration/env";

export const useHttp = (model: string, applyData: (_: any) => void) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const load = useCallback(async () => {
    setIsLoading(true);
    setError(null);
    try {
      const response = await fetch(env.server + model + ".json");
      if (!response.ok) {
        throw new Error(response.statusText || "Faild to fetch data");
      }
      const responseJSON = await response.json();
      applyData(responseJSON);

      setIsLoading(false);
    } catch (err) {
      setIsLoading(false);
      setError(err);
    }
  }, [model, applyData]);

  const post = useCallback(
    async (data: {}, applyCallback: (_: {}) => void) => {
      setIsLoading(true);
      setError(null);
      try {
        const response = await fetch(env.server + model + ".json", {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            "Content-Type": "application/json",
          },
        });
        if (!response.ok) {
          throw new Error(response.statusText || "Faild to fetch data");
        }
        const responseJSON = await response.json();
        applyCallback(responseJSON);

        setIsLoading(false);
      } catch (err) {
        setIsLoading(false);
        setError(err);
      }
    },
    [model]
  );

  return {
    load,
    post,
    isLoading,
    error,
  };
};

export default useHttp;
