import "./App.css";
import { Route, Switch } from "react-router-dom";
import { AllMeetupsPage } from "./pages/AllMeetupsPage";
import { NewMeetupPage } from "./pages/NewMeetupPage";
import { FavoritesPage } from "./pages/FavoristesPage";
import { Layout } from "./components/layout/Layout";
import React from "react";

const App: React.FC = () => {
  return (
    <Layout>
      <Switch>
        <Route path="/" exact>
          <AllMeetupsPage />
        </Route>
        <Route path="/new-meetup">
          <NewMeetupPage />
        </Route>
        <Route path="/favorites">
          <FavoritesPage />
        </Route>
      </Switch>
    </Layout>
  );
};

export default App;
