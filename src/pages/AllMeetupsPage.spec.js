import { render, screen, waitFor } from "@testing-library/react";

// mock use-meetups
jest.mock("../hooks/services/UseMeetups");
import useMeetups from "../hooks/services/UseMeetups";
import { AllMeetupsPage } from "./AllMeetupsPage";

const fakeMeetupData = [
  {
    id: "3",
    title: "Short title",
    description: "short description",
    image: "https://upload.wikimedia.org/jpg",
    address: "short address",
  },
  {
    id: "4",
    title: "Long title",
    description: "Long description 4",
    image: "https://upload.wikimedia.org/png",
    address: "Long address",
  },
];

test("Loading meetups",() => {
  // MOCK //
  useMeetups.mockReturnValue({
    fetchMeetups: () => {},
    meetups: [],
    isLoading: true,
    error: null,
  });

  // RENDER //
  render(
    <AllMeetupsPage />
  );


  // VALIDATION //
  const loadingElt = screen.getByText(/loading.../i);
  expect(loadingElt).toBeInTheDocument();
});

test("meetups displayed", async () => {
  // MOCK //
  const mockFetchMeetups = jest.fn();
  useMeetups.mockReturnValue({
    fetchMeetups: mockFetchMeetups,
    meetups: fakeMeetupData,
    isLoading: false,
    error: null,
  });

  // RENDER //
  await render(<AllMeetupsPage />);

  // VALIDATION //
  expect(mockFetchMeetups).toHaveBeenCalledTimes(1);
  fakeMeetupData.forEach((data) => {
    expect(screen.getByText(data.title)).toBeInTheDocument();
  });
});
