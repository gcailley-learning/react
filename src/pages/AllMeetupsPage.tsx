import React, { useEffect } from "react";
import { MeetupList } from "../components/meetups/MeetupList";
import { useMeetups } from "../hooks/services/UseMeetups";

export const AllMeetupsPage: React.FC = () => {
  const { fetchMeetups, meetups, isLoading, error } = useMeetups();

  useEffect(() => {
    fetchMeetups();
  }, [fetchMeetups]);

  if (isLoading) {
    return (
      <section>
        <p>loading...</p>
      </section>
    );
  } else {
    return (
      <section>
        <h1>All Meetups</h1>
        <MeetupList meetups={meetups} />
        {error && <span>{error}</span>}
      </section>
    );
  }
};
