import React from "react";
import { useHistory } from "react-router-dom";
import { NewMeetupForm } from "../components/meetups/NewMeetupFrom";
import { useMeetups } from "../hooks/services/UseMeetups";
import { MeetupItemNotSavedModel } from "../models/MeetupItemModel";

export const NewMeetupPage: React.FC = () => {
  const history = useHistory();
  const meetupService = useMeetups();

  const onAddMeetup = (meetup: MeetupItemNotSavedModel) => {
    meetupService.postMeetups(meetup, (_response: {}) => {
      history.replace("/");
    });
  };

  return (
    <section>
      <h1>New Meetup Page</h1>
      <NewMeetupForm onAddMeetup={onAddMeetup} />
    </section>
  );
};
