import React, { useContext } from "react";
import { MeetupList } from "../components/meetups/MeetupList";
import { FavoritesContext } from "../store/FavoritesContext";

export const FavoritesPage: React.FC = () => {
  const favoriteCtx = useContext(FavoritesContext);

  let content;
  if (favoriteCtx.totalFavorites === 0) {
    content = <p>You got no favorites yet.</p>;
  } else {
    content = <MeetupList meetups={favoriteCtx.favorites} />;
  }
  return (
    <section>
      <h1>Your Favorites Meetups</h1>
      {content}
    </section>
  );
};
