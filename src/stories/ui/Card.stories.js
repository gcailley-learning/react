import React from 'react';

import Card from '../../components/ui/card/Card';

export default {
  title: 'UI/Card',
  component: Card,
};

const Template = (args) => <Card {...args} >My Card</Card>;

export const card = Template.bind({});

