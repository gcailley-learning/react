import React from "react";

import Layout from "../../components/layout/Layout";
import { MemoryRouter } from "react-router-dom";

export default {
  title: "Layout/Layout",
  component: Layout,
};

const Template = (args) => {
  return (
    <MemoryRouter>
      <Layout {...args} />
    </MemoryRouter>
  );
};

export const layout = Template.bind({});
layout.args = {};
