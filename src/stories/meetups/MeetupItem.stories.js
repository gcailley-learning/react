import React from "react";
import MeetupItem from "../../components/meetups/MeetupItem";

export default {
  title: "Meetup/MeetupItem",
  component: MeetupItem,
};

const Template = (args) => <ul><MeetupItem {...args}></MeetupItem></ul>;

export const meetupItem = Template.bind({});
meetupItem.args = {
  id: "3",
  title: "short title",
  description: "short description",
  image: "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Stadtbild_M%C3%BCnchen.jpg/2560px-Stadtbild_M%C3%BCnchen.jpg",
  address: "short address",
};
