import React from "react";
import MeetupList from "../../components/meetups/MeetupList";

export default {
  title: "Meetup/MeetupList",
  component: MeetupList,
};

const Template = (args) => <MeetupList {...args}></MeetupList>;

export const meetupListNoData = Template.bind({});
meetupListNoData.args = {
  meetups: [],
};

export const meetupListWithData = Template.bind({});
meetupListWithData.args = {
  meetups: [
    {
      id: "3",
      title: "short title",
      description: "short description",
      image:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Stadtbild_M%C3%BCnchen.jpg/2560px-Stadtbild_M%C3%BCnchen.jpg",
      address: "short address",
    },
    {
      id: "4",
      title: "short title 4",
      description: "short description 4",
      image:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Stadtbild_M%C3%BCnchen.jpg/2560px-Stadtbild_M%C3%BCnchen.jpg",
      address: "short address",
    },
  ],
};
