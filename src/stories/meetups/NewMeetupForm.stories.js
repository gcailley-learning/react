import React from "react";
import { NewMeetupForm } from "../../components/meetups/NewMeetupFrom";

export default {
  title: "Meetup/NewMeetupForm",
  component: NewMeetupForm,
};

const Template = (args) => <NewMeetupForm {...args}></NewMeetupForm>;

export const newMeetupForm = Template.bind({});
newMeetupForm.args = {
  onAddMeetup: () =>{console.log('add')},
};
