export interface MeetupItemNotSavedModel {
  title: string;
  description: string;
  image: string;
  address: string;
}

export interface MeetupItemModel extends MeetupItemNotSavedModel {
  id: string;
}
