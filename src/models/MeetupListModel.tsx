import { MeetupItemModel } from "./MeetupItemModel";

export interface MeetupListModel {
    meetups : MeetupItemModel[]
  }
  