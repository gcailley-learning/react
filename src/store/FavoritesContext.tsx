import React, { createContext, useState } from "react";
import { MeetupItemModel } from "../models/MeetupItemModel";

const favoritesInit: MeetupItemModel[] = [];
export const FavoritesContext = createContext({
  favorites: favoritesInit,
  totalFavorites: 0,
  addFavorite: (_favoriteMeetup: MeetupItemModel): void => {},
  removeFavorite: (_meetupId: string): void => {},
  itemIsFavorite: (_meetupId: string): boolean => {
    return false;
  },
});
export interface FavoritesContextProviderProps {
  children: any;
}
export const FavoritesContextProvider: React.FC<FavoritesContextProviderProps> = (
  props
) => {
  const [userFavorites, setUserFavorites] = useState<MeetupItemModel[]>([]);

  const addFavoriteHandler = (favoriteMeetup: MeetupItemModel): void => {
    // behind the scene, les changements de stats ne sont pas synchro
    // donc potentiellement on peut avoir une mauvaise valeur
    // cette fonction garantie que le previous snapshot est à jour
    setUserFavorites((prevUserFavorites) => {
      return prevUserFavorites.concat(favoriteMeetup);
    });
  };

  const removeFavoriteHandler = (meetupId: string): void => {
    setUserFavorites((prevUserFavorites) => {
      return prevUserFavorites.filter((meetup) => meetup.id !== meetupId);
    });
  };

  const itemIsFavoriteHandler = (meetupId: string): boolean => {
    return userFavorites.some((meetup) => meetup.id === meetupId);
  };

  const context = {
    favorites: userFavorites,
    totalFavorites: userFavorites.length,
    addFavorite: addFavoriteHandler,
    removeFavorite: removeFavoriteHandler,
    itemIsFavorite: itemIsFavoriteHandler,
  };

  return (
    <FavoritesContext.Provider value={context}>
      {props.children}
    </FavoritesContext.Provider>
  );
};

export default FavoritesContext;
