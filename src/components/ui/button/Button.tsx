import React, { CSSProperties } from "react";
import classes from "./Button.module.css";

export interface ButtonProps {
  label: string;
  primary?: boolean;
  backgroundColor?: string;
  size?: "small" | "medium" | "large";
  buttonType?: "button" | "submit" | "reset" | undefined;
  disabled?:boolean;
  onClick?: (_event: any) => void;
}

/**
 * Primary UI component for user interaction
 */
export const Button: React.FC<ButtonProps> = ({
  primary = false,
  buttonType = "button",
  backgroundColor,
  size,
  label,
  disabled = false,
  onClick,
}) => {
  const mode = primary
    ? classes["storybook-button-primary"]
    : classes["storybook-button-secondary"];

  const backgroundColorProperties: CSSProperties = {
    backgroundColor: backgroundColor,
  };

  return (
    <button
      type={buttonType}
      className={[
        classes["storybook-button"],
        classes[`storybook-button-${size}`],
        mode,
      ].join(" ")}
      style={backgroundColorProperties}
      onClick={onClick}
      disabled={disabled}
    >
      { label }
    </button>
  );
};

export default Button;
