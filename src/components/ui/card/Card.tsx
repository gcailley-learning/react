import React from "react";
import classes from "./Card.module.css";

export interface CardProps {
  children: any;
}

export const Card: React.FC<CardProps> = (props) => {
  return <div className={classes.card}>{props.children}</div>;
};

export default Card;
