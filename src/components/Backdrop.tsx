export interface BackdropProps {
  onCancel: any;
}

export const Backdrop: React.FC<BackdropProps> = (props) => {
  return <div className="backdrop" onClick={props.onCancel} />;
};

export default Backdrop;
