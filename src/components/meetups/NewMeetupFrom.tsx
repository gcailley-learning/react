import React, { FormEvent, useEffect, useState } from "react";

import { Card } from "../ui/card/Card";
import {Button} from "../ui/button/Button";
import { MeetupItemNotSavedModel } from "../../models/MeetupItemModel";
import classes from "./NewMeetupForm.module.css";

export interface NewMeetupFormProps {
  onAddMeetup: (_data: MeetupItemNotSavedModel) => void;
}
export const NewMeetupForm: React.FC<NewMeetupFormProps> = (props) => {
  const [formInputs, setFormInputs] = useState<MeetupItemNotSavedModel>({
    title: "",
    image: "",
    address: "",
    description: "",
  });

  const [formValide, setFormValide] = useState(false);

  const onSubmitHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    // VALIDATION //
    props.onAddMeetup(formInputs);
  };

  useEffect(() => {
    // Validation form en live
    setFormValide(
      formInputs.title.trim() !== "" &&
        formInputs.image.trim() !== "" &&
        formInputs.address.trim() !== "" &&
        formInputs.description.trim() !== ""
    );
  }, [formInputs]);

  useEffect(() => {
    // call
    if (formValide) {
      console.log("Formulaire Valide");
    } else {
      console.log("Formulaire Invalide");
    }
  }, [formValide]);

  return (
    <Card>
      <form className={classes.form} onSubmit={onSubmitHandler}>
        <div className={classes.control}>
          <label htmlFor="title">Meetup Title</label>
          <input
            type="text"
            required
            id="title"
            value={formInputs.title}
            onChange={(e) =>
              setFormInputs({ ...formInputs, title: e.currentTarget.value })
            }
          />
        </div>

        <div className={classes.control}>
          <label htmlFor="image">Meetup Image</label>
          <input
            type="url"
            required
            id="image"
            value={formInputs.image}
            onChange={(e) =>
              setFormInputs({ ...formInputs, image: e.currentTarget.value })
            }
          />
        </div>

        <div className={classes.control}>
          <label htmlFor="address">Meetup Address</label>
          <input
            type="text"
            required
            id="address"
            value={formInputs.address}
            onChange={(e) =>
              setFormInputs({ ...formInputs, address: e.currentTarget.value })
            }
          />
        </div>

        <div className={classes.control}>
          <label htmlFor="description">Meetup Description</label>
          <textarea
            required
            id="description"
            rows={5}
            value={formInputs.description}
            onChange={(e) =>
              setFormInputs({
                ...formInputs,
                description: e.currentTarget.value,
              })
            }
          />
        </div>

        <div className={classes.actions}>
          <Button
            buttonType="submit"
            label="Add Meetup"
            size="large"
            primary
            disabled={!formValide}
          />
        </div>
      </form>
    </Card>
  );
};
export default NewMeetupForm;