import { render, screen } from "@testing-library/react";
import { MeetupItem } from "./MeetupItem";

test("renders learn react link", () => {
  const expectedId = "3";
  const expectedTitle = "expected Title";
  const expectedDescription = "expected Description";
  const expectedAddress = "expected Address";
  render(
    <MeetupItem
      id={expectedId}
      title={expectedTitle}
      description={expectedDescription}
      image=""
      address={expectedAddress}
    />
  );
  const titleElt = screen.getByText(expectedTitle);
  expect(titleElt).toBeInTheDocument();

  const titleDesc = screen.getByText(expectedDescription);
  expect(titleDesc).toBeInTheDocument();

  const addressElt = screen.getByText(expectedAddress);
  expect(addressElt).toBeInTheDocument();

});
