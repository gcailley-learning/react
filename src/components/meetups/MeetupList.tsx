import React from "react";

import classes from "./MeetupList.module.css";
import { MeetupItem } from "./MeetupItem";
import { MeetupListModel } from "../../models/MeetupListModel";


export const MeetupList: React.FC<MeetupListModel> = ({
  meetups
}) => {
  if (meetups.length === 0) {
    return (
      <section>
        <span>Pas de données disponibles</span>
      </section>
    );
  } else {
    return (
      <ul className={classes.list}>
        {meetups.map((meetup) => (
          <MeetupItem
            key={meetup.id}
            id={meetup.id}
            title={meetup.title}
            image={meetup.image}
            address={meetup.address}
            description={meetup.description}
          />
        ))}
      </ul>
    );
  }
};

// MeetupList.propTypes = {
//   /**
//    * list des meetups à afficher.
//    */
//   meetups: PropTypes.arrayOf(MeetupItemProps),
// };

// MeetupList.defaultProps = {
//   meetups: [],
// };


export default MeetupList;