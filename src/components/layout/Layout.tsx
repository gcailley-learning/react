import React from "react";
import classes from "./Layout.module.css";
import MainNavigation from "./MainNavigation";

export interface LayoutProps {
  children: any;
}

export const Layout:React.FC<LayoutProps> = (props) => {
  return (
    <div className={classes.main}>
      <MainNavigation />
      {props.children}
    </div>
  );
}

export default Layout;
