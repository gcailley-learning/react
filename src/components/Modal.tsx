import React from "react";

export interface ModalProps {
  onCancel: any;
  onConfirm: any;
}
export const Modal: React.FC<ModalProps> = (props) => {
  return (
    <div className="modal">
      <p>Are you sure ? </p>
      <button className="btn btn--alt" onClick={props.onCancel}>
        Cancel
      </button>
      <button className="btn" onClick={props.onConfirm}>
        Confirm
      </button>
    </div>
  );
};
